package net.chris.ct.chess;

import java.util.List;

import net.chris.ct.chess.validator.ChessValidator;
import net.chris.ct.chess.validator.ValidationResult;

public class ChessBoard {

    private List<ChessValidator> validators;

    public ChessBoard(final List<ChessValidator> validators) {
        this.validators = validators;
    }

    public String decodeBoard(final String encodedBoard) {

        String decodedBoard;
        final ValidationResult validationResult = validate(encodedBoard);

        if (validationResult.hasError()) {
            decodedBoard = validationResult.getError().getErrorMessage();
        } else {
            final StringBuffer buffer = decodeValidBoard(encodedBoard);

            decodedBoard = buffer.toString();
        }

        return decodedBoard;
    }

    private StringBuffer decodeValidBoard(final String encodedBoard) {
        final StringBuffer buffer = new StringBuffer();

        for (final String row : encodedBoard.split("/")) {

            final StringBuffer decodedRow = new StringBuffer();

            for (int i = 0; i < row.length(); i++) {
                final char boardCharacter = row.charAt(i);

                convertCharacter(decodedRow, boardCharacter);
            }

            decodedRow.append("\r\n");
            buffer.append(decodedRow);
        }
        return buffer;
    }

    private void convertCharacter(final StringBuffer decodedRow, final char boardCharacter) {
        if (Character.isLetter(boardCharacter)) {
            decodedRow.append(boardCharacter);
        } else {
            final int numberOfSpaces = Character.getNumericValue(boardCharacter);

            for (int j = 0; j < numberOfSpaces; j++) {
                decodedRow.append(".");
            }
        }
    }

    private ValidationResult validate(final String encodedBoard) {

        ValidationResult validationResult = ValidationResult.success();

        for (final ChessValidator validator : validators) {
            validationResult = validator.validate(encodedBoard);

            if (validationResult.hasError()) {
                break;
            }
        }
        return validationResult;
    }
}
