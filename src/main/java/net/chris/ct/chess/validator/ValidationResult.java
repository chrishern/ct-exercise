package net.chris.ct.chess.validator;

public class ValidationResult {

    private boolean hasError;
    private ValidationError error;

    private ValidationResult(final boolean hasError) {
        this.hasError = hasError;
    }

    private ValidationResult(final boolean hasError, final ValidationError error) {
        this.hasError = hasError;
        this.error = error;
    }

    public boolean hasError() {
        return hasError;
    }

    public ValidationError getError() {
        return error;
    }

    public void setError(ValidationError error) {
        this.error = error;
    }

    public static ValidationResult success() {
        return new ValidationResult(false);
    }

    public static ValidationResult withError(final ValidationError error) {
        return new ValidationResult(true, error);
    }
}
