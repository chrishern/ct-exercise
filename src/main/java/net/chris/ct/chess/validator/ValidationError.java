package net.chris.ct.chess.validator;

public enum ValidationError {

    // @formatter:off
    INVALID_NUMBER_OF_ROWS_ERROR("Invalid encoded position: Invalid number of rows"),
    INVALID_NUMBER_OF_SQUARES_ERROR("Invalid encoded position: Invalid number of characters in row"),
    UNEXPECTED_CHARACTER_ERROR("Invalid encoded position: Unexpected character");
    // @formatter:on

    private String errorMessage;

    private ValidationError(final String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

}
