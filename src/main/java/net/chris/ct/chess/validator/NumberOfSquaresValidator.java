package net.chris.ct.chess.validator;

public class NumberOfSquaresValidator implements ChessValidator {

    private final static int VALID_NUMBER_OF_SQUARES = 8;

    @Override
    public ValidationResult validate(final String encodedBoard) {

        for (final String row : encodedBoard.split("/")) {

            int numberInRow = 0;

            for (int i = 0; i < row.length(); i++) {
                final char boardCharacter = row.charAt(i);

                if (Character.isLetter(boardCharacter)) {
                    numberInRow++;
                } else {
                    numberInRow += Character.getNumericValue(boardCharacter);
                }
            }

            if (numberInRow != VALID_NUMBER_OF_SQUARES) {
                return ValidationResult.withError(ValidationError.INVALID_NUMBER_OF_SQUARES_ERROR);
            }
        }

        return ValidationResult.success();
    }

}
