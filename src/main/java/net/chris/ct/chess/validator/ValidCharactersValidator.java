package net.chris.ct.chess.validator;

import com.google.common.base.CharMatcher;

public class ValidCharactersValidator implements ChessValidator {

    private final static String VALID_CHARACTERS = "rbkpqnRBKPQN12345678";

    private CharMatcher matcher;

    public ValidCharactersValidator() {
        matcher = CharMatcher.anyOf(VALID_CHARACTERS);
    }

    @Override
    public ValidationResult validate(final String encodedBoard) {

        for (final String row : encodedBoard.split("/")) {
            final boolean matchesAllOf = matcher.matchesAllOf(row);

            if (!matchesAllOf) {
                return ValidationResult.withError(ValidationError.UNEXPECTED_CHARACTER_ERROR);
            }
        }

        return ValidationResult.success();
    }

}
