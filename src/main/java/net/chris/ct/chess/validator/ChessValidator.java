package net.chris.ct.chess.validator;

public interface ChessValidator {

    ValidationResult validate(String encodedBoard);
}
