package net.chris.ct.chess.validator;

public class NumberOfRowsValidator implements ChessValidator {

    private final static int VALID_NUMBER_OF_ROWS = 8;

    @Override
    public ValidationResult validate(final String encodedBoard) {

        final String[] rows = encodedBoard.split("/");

        ValidationResult result;

        if (rows.length == VALID_NUMBER_OF_ROWS) {
            result = ValidationResult.success();
        } else {
            result = ValidationResult.withError(ValidationError.INVALID_NUMBER_OF_ROWS_ERROR);
        }

        return result;
    }

}
