package net.chris.ct.chess;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import net.chris.ct.chess.validator.ChessValidator;
import net.chris.ct.chess.validator.ValidationError;
import net.chris.ct.chess.validator.ValidationResult;

@RunWith(MockitoJUnitRunner.class)
public class ChessBoardTest {

    private ChessBoard board;

    @Mock
    private ChessValidator validator;

    @Before
    public void setup() {
        board = new ChessBoard(Arrays.asList(validator));
    }

    @Test
    public void testDecode_ValidInput() {
        // arrange
        final String input = "r1bk3r/p2pBpNp/n4n2/1p1NP2P/6P1/3P4/P1P1K3/q5b1";
        when(validator.validate(input)).thenReturn(ValidationResult.success());

        // act
        final String decodedBoard = board.decodeBoard(input);

        // assert
        assertThat(decodedBoard)
                .isEqualTo("r.bk...r\r\np..pBpNp\r\nn....n..\r\n.p.NP..P\r\n......P.\r\n...P....\r\nP.P.K...\r\nq.....b.\r\n");
    }

    @Test
    public void testDecode_InvalidInput() {
        // arrange
        final String input = "r1bk3r/p2pBpNp/n4n2/1p1NP2P/6P1/3P4/P1P1K3";
        when(validator.validate(input)).thenReturn(ValidationResult.withError(ValidationError.INVALID_NUMBER_OF_ROWS_ERROR));

        // act
        final String decodedBoard = board.decodeBoard(input);

        // assert
        assertThat(decodedBoard).isEqualTo(ValidationError.INVALID_NUMBER_OF_ROWS_ERROR.getErrorMessage());
    }
}
