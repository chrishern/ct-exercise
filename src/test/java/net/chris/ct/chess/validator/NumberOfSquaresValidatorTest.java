package net.chris.ct.chess.validator;

import static org.fest.assertions.Assertions.assertThat;

import org.junit.Before;
import org.junit.Test;

public class NumberOfSquaresValidatorTest {

    private NumberOfSquaresValidator validator;

    @Before
    public void setup() {
        validator = new NumberOfSquaresValidator();
    }

    @Test
    public void testValidate_Valid() {
        // arrange
        final String board = "r1bk3r/p2pBpNp/n4n2/1p1NP2P/6P1/3P4/P1P1K3/q5b1";

        // act
        final ValidationResult result = validator.validate(board);

        // assert
        assertThat(result.hasError()).isFalse();
    }

    @Test
    public void testValidate_TooManySquares() {
        // arrange
        final String board = "r1bk3r/p2pBpNp/n4n2/1p1NP2P/6P1/3P4/P1P1K3/q5b5";

        // act
        final ValidationResult result = validator.validate(board);

        // assert
        assertThat(result.hasError()).isTrue();
        assertThat(result.getError()).isEqualTo(ValidationError.INVALID_NUMBER_OF_SQUARES_ERROR);
    }

    @Test
    public void testValidate_TooFewRows() {
        // arrange
        final String board = "r1bk3r/p2pBpNp/n4n2/1p1NP2P/6P1/3P4/P1P1K3/q5b";

        // act
        final ValidationResult result = validator.validate(board);

        // assert
        assertThat(result.hasError()).isTrue();
        assertThat(result.getError()).isEqualTo(ValidationError.INVALID_NUMBER_OF_SQUARES_ERROR);
    }
}
